# Task Management App

## Nhu cầu thực tế

Cần có một ứng dụng để cho quản lý công việc cá nhân.
Ứng dụng có thể sử dụng mà không cần kết nối mạng và sử dụng mã PIN để bảo mật.

## Yêu cầu chi tiết

- [ ] Clone git, tạo nhánh mới từ nhánh main. Dùng Flutter để xây dựng ứng dụng.
- [ ] Sử dụng một mã PIN để truy cập vào ứng dụng. Lần đầu vào ứng dụng thì sẽ phải set mã PIN và có thể thay đổi sau. 
- [ ] Tạo một công việc với các thông tin cơ bản: Tiêu đề, nội dung, deadline.
- [ ] Một task có thể có ds các đầu việc cần phải làm.
- [ ] Xem được tất cả các ghi chú đã được tạo dưới dạng danh sách.
- [ ] Cập nhật trạng thái của ghi chú bằng checkbox.
- [ ] Mọi dữ liệu được lưu dưới máy bằng [SharedPreferences](https://pub.dev/packages/shared_preferences).

## Git

Có thể sử dụng commandline, git tích hợp sẵn trên IDE hoặc phần mềm [SourceTree](https://www.sourcetreeapp.com/)

## UI/UX

Sử dụng phần mềm Figma, dựa theo UI/UX chính [tại đây](https://www.figma.com/file/g5Z6cJsF8F0oQoCn1T1DX1/%5BMobile%5D-Task-Management-App?t=FIH9AHBbWVLy9aXa-1)
Một số màn hình chưa có UI/UX trên figma thì có thể tự do xây dựng UI/UX miễn phù hợp với yêu cầu.


